module.exports = class {
  constructor(url, fetch) {
    this.url = url;
    this.fetch = fetch;
  }

  async get(resource, headers) {
    return await this.fetch(`${this.url}/${resource}`, {headers});
  }

  async head(resource, headers) {
    return await this.fetch(`${this.url}/${resource}`, {
      method: 'HEAD',
      headers
    });
  }

  async put(...args) {
    return await this._request('PUT', ...args);
  }

  async post(...args) {
    return await this._request('POST', ...args);
  }

  authorization(token) {
    return {'authorization': `Bearer ${token}`};
  }

  async _request(method, resource, ...args) {
    const body = args.pop();
    const [headers] = args;

    return await this.fetch(`${this.url}/${resource}`, {
      method,
      headers: {
        ...headers,
        'content-type': 'application/json'
      },
      body: JSON.stringify(body)
    });
  }
}
